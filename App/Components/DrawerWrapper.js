import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Alert, View, Text } from 'react-native';
import { Drawer } from 'native-base';
import styles from './Styles/DrawerWrapperStyle'

import DrawerContent from './DrawerContent';

export default class DrawerWrapper extends Component {
  constructor ( props ) {
    super( props );

    this.state = {
      toggle: 'hide'
    };
  }

  componentWillReceiveProps ( nextProps ) {
    if ( nextProps.toggle !== this.state.toggle ) {
      this.setState( {
        'toggle' : nextProps.toggle
      } );

      if ( nextProps.toggle === 'show' && this.drawer ) {
        this.drawer._root.open();
      } else {
        this.drawer._root.close();
      }
    }
  }

  render () {
    const { navigate } = this.props.navigation;

    return (
      <Drawer
        style={{ position: 'absolute', top: -50, left: 0, right: 0, bottom: 0 }}
        onClose={ () => this.props.closeDrawer() }
        content={ <DrawerContent signOut={ () => this.props.signOut( navigate ) } navigate={ navigate } /> }
        ref={ ( ref ) => this.drawer = ref }>
        { this.renderChildren() }
      </Drawer>
    )
  }
}
