import React, { Component } from 'react'
import { BackHandler } from 'react-native'
import { connect } from 'react-redux';
import { View, Text, Alert, ActivityIndicator } from 'react-native'
import styles from './Styles/MyMeetingsStyle'

import Creators from '../Redux/MeetingsRedux';
import DrawerCreators from '../Redux/DrawerRedux';
import AuthenticationCreators from '../Redux/AuthenticationRedux';

import FloatingButton from './FloatingButton';

import MeetingList from './MeetingList';
import DrawerWrapper from './DrawerWrapper';

class MyMeetings extends DrawerWrapper {
  constructor ( props ) {
    super( props );

    this.state = {
      fetching: false,
      meetings: [],
      mode: 'normal',
    };
  }

  componentDidMount () {
    this.props.getMeetings( this.props.navigation.navigate );
    BackHandler.addEventListener( 'hardwareBackPress', this.onBackPress );
  }

  componentWillUnmount () {
    BackHandler.removeEventListener( 'hardwareBackPress', this.onBackPress );
  }

  componentWillReceiveProps ( nextProps ) {
    super.componentWillReceiveProps( nextProps );

    this.setState( {
      meetings: nextProps.meetings,
      fetching: nextProps.fetching,
    } );
  }

  onBackPress = () => {
    this.setState( { mode: 'normal' } );

    return false;
  }

  onItemPressed = ( item ) => {
    if ( this.state.mode === 'selection' ) {
      const { meetings } = this.state;

      const mapped = meetings.map( ( meeting ) => {
        if ( meeting.no == item.no ) {
          return {
            ...meeting,
            'selected' : !meeting.selected
          };
        }

        return meeting;
      } );

      this.setState( { 'meetings' : mapped } );
    } else {
      this.props.navigation.navigate( 'MeetingViewScreen', { item } );
    }
  }

  onItemLongPressed = ( item ) => {
    if ( this.state.mode === 'selection' ) {
      const { meetings } = this.state;

      const mapped = meetings.map( ( meeting ) => {
        return {
          ...meeting,
          selected: false
        };
      } );

      return this.setState( {
        'mode'     : 'normal',
        'meetings' : mapped
      } );
    }

    const { meetings } = this.state;

    const mapped = meetings.map( ( meeting ) => {
      if ( meeting.no == item.no ) {
        return {
          ...meeting,
          'selected' : true
        };
      }

      return meeting;
    } );

    this.setState( {
      'meetings': mapped,
      'mode'     : 'selection'
    } );
  }

  onCheckboxValueChanged = ( item, value ) => {
    const { meetings } = this.state;

    const mapped = meetings.map( ( meeting ) => {
      if ( meeting.no == item.no ) {
        return {
          ...meeting,
          'selected' : value
        };
      }

      return meeting;
    } );

    this.setState( { 'meetings' : mapped } );
  }

  renderLoading = () => {
    if ( this.state.fetching !== 'done' ) {
      return <View style={ styles.loadingContainerStyle }>
          <ActivityIndicator animating={ this.state.fetching !== 'done' } size="large" color="#ff1744" />
        </View>;
    }

    return null;
  }

  renderFloatingButton = () => {
    if ( this.state.mode !== 'normal' ) {
      return <FloatingButton icon="trash" />;
    } else {
      return <FloatingButton onPress={ () => this.props.navigation.navigate( 'AddMeetingScreen' ) } icon="add" />;
    }
  }

  renderChildren () {
    const { meetings } = this.state;

    return (
      <View style={styles.container}>
        { this.renderLoading() }
        <MeetingList
          mode={ this.state.mode }
          onItemPressed={ this.onItemPressed }
          onItemLongPressed={ this.onItemLongPressed }
          onCheckboxValueChanged={ this.onCheckboxValueChanged }
          list={ meetings } />
          { this.renderFloatingButton() }
      </View>
    )
  }
}

const mapStateToProps = ( state ) => {
  return {
    fetching: state.meetings.fetching,
    meetings: state.meetings.payload.data || [],
    error: state.meetings.error,
    toggle: state.drawer.toggle
  }
}

const mapDispatchToProps = ( dispatch ) => {
  return {
    getMeetings ( navigate ) {
      dispatch( Creators.meetingsRequest( navigate ) );
    },
    toggleDrawer () {
      dispatch( DrawerCreators.toggleDrawer() );
    },
    closeDrawer () {
      dispatch( DrawerCreators.closeDrawer() );
    },
    signOut ( navigate ) {
      dispatch( AuthenticationCreators.signOut( navigate ) );
    }
  }
}

export default connect( mapStateToProps, mapDispatchToProps ) ( MyMeetings );
