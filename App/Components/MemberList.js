import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity } from 'react-native'
import { Icon } from 'native-base';
import styles from './Styles/MemberListStyle'

const MemberListItem = ( props ) => {
  return <View style={ styles.memberListItemContainer }>
    <View style={ styles.personLogoContainer }>
      <Icon color="#727373" name="person" />
    </View>
    <View style={ styles.personName }>
      <Text>{ props.name }</Text>
    </View>
    <View style={ styles.remove }>
      <TouchableOpacity onPress={ () => props.onAttendeeRemoved( props.UserId ) }>
        <Icon color="#727373" name="remove-circle" />
      </TouchableOpacity>
    </View>
  </View>;
};

const MemberList = ( props ) => {
  const { attendees } = props;

  return <View style={styles.container}>
    { attendees.map( ( attendee, index ) => <MemberListItem onAttendeeRemoved={ props.onAttendeeRemoved } key={ `attendee-${ index }` } name={ attendee.Name } UserId={ attendee.UserId } /> ) }
  </View>;
}

MemberList.propTypes = {
  attendees: PropTypes.array,
  onAttendeeRemoved: PropTypes.func,
};


MemberList.defaultProps = {
  attendees: []
};

export default MemberList;
