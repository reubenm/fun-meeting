import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    width: 50,
    height: 50,
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.1)',
    flexDirection: 'column',
  },
  monthContainer: {
    height: 20,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FF5252',
  },
  dayContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    justifyContent: 'center',
  },
  dayText: {
    fontSize: 16,
  },
  monthText: {
    fontSize: 8,
    color: '#fff'
  }
})
