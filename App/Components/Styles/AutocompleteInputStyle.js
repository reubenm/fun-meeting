import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    paddingBottom: 10,
  },
  resultsContainer: {
    position: 'absolute',
    top: 55,
    left: 0,
    right: 0,
    minHeight: 50,
    maxHeight: 250,
    marginLeft: 20,
    marginRight: 20,
    borderWidth: 1,
    borderColor: '#dddae0',
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    zIndex: 3,
  },
  resultItemContainer: {
    minHeight: 50,
    justifyContent: 'center',
    paddingLeft: 10,
    paddingRight: 10,
  },
  inputStyle: {
    flex: 1,
    borderWidth: 0,
  },
})
