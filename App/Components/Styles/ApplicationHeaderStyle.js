import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 0,
    height: 40,
    backgroundColor: 'blue',
  },
  headerStyle: {
    height: 40,
    backgroundColor: '#F50057',
  },
  funmeetingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  funMeetingText: {
    fontFamily: 'Cookie-Regular',
    fontSize: 25,
    color: '#fff',
  },
  menuContainer: {
    width: 50,
    position: 'absolute',
    left: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
})
