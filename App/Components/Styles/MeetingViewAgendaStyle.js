import { StyleSheet } from 'react-native'
import {Fonts} from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
    fontFamily: Fonts.type.base,
  },
  subTitleText: {
    fontSize: 16,
    color: '#808080',
    fontFamily: Fonts.type.base,
  }
})
