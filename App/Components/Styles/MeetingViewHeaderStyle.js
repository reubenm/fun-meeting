import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    minHeight: 70,
    flexDirection: 'row',
    backgroundColor: '#2196F3',
    alignItems: 'center',
    paddingLeft: 5,
    paddingRight: 5,
  },
  topButtonsContainer: {
    height: 20,
    width: 20,
  },
  titleStyle: {
    color: '#fff',
    fontSize: 20,
    paddingLeft: 10,
  }
})
