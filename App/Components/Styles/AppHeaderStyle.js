import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    height: 80,
    alignItems: 'center',
    flexDirection: 'row'
  },
  headerBackgroundStyle: {
    position: 'absolute',
    width: 420,
    height: 100,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  profilePictureContainer: {
    backgroundColor: '#fff',
    marginLeft: 15 ,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    width: 50,
    height: 50,
    overflow: 'hidden',
  },
  profilePicture: {
    width: 50,
    height: 50,
  },
  rightItemsContainer: {
    height: 50,
    justifyContent: 'center',
    marginLeft: 15
  },
  nameText: {
    fontSize: 20,
    color: '#fff',
    fontWeight: '500'
  }
})
