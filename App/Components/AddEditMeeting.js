import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, TouchableOpacity } from 'react-native'
import styles from './Styles/AddEditMeetingStyle'
import { Container, Header, Content, Form, Item, Input, Label, Button, Text, Card, Body, CardItem } from 'native-base';
import DatePicker from 'react-native-datepicker';

import MemberList from './MemberList';
import FloatingButton from './FloatingButton';
import AutocompleteInput from './AutocompleteInput';

const createError = ( error ) => {
  if ( error && error.length ) {
    return <View style={ styles.errorMessageContainer }>
      <Text style={ styles.errorMessageStyle }>{ error[ 0 ] }</Text>
    </View>
  }
}

const AddEditMeeting = ( props ) => {
  const {
    data,
    filteredUsers,
    textValueChanged,
    onAttendeeRemoved,
    canDisplayErrors,
    nameError,
    agendaError,
    locationError,
    urlError
  } = props;

  return <Container style={ styles.container }>
    <Content>
        <Card>
          <CardItem>
            <Body style={{ alignItems: 'stretch' }}>
              <Form style={ styles.formStyle }>
                <Item error={ canDisplayErrors && nameError && nameError.length > 0 } style={ styles.itemStyle } stackedLabel>
                  <Label>Name</Label>
                  <Input onChangeText={ ( text ) => textValueChanged( 'name', text ) } />
                </Item>
                { createError( nameError ) }
                <Item error={ canDisplayErrors && agendaError && agendaError.length > 0 } style={ styles.itemStyle } stackedLabel>
                  <Label>Agenda</Label>
                  <Input onChangeText={ ( text ) => textValueChanged( 'agenda', text ) } />
                </Item>
                { createError( agendaError ) }
                <Item style={ styles.itemStyle }>
                  <Label>Schedule</Label>
                  <View style={ styles.datepickerContainer }>
                    <DatePicker
                      mode="datetime"
                      format="YYYY-MM-DD hh:mm a"
                      style={ styles.datepicker }
                      onDateChange={ props.onDateValueChanged }
                      date={ data.schedule } />
                  </View>
                </Item>
                <Item error={ canDisplayErrors && locationError && locationError.length > 0 } style={ styles.itemStyle } stackedLabel>
                  <Label>Location</Label>
                  <Input onChangeText={ ( text ) => textValueChanged( 'location', text ) } />
                </Item>
                { createError( locationError ) }
                <Item error={ canDisplayErrors && urlError && urlError.length > 0 } style={ styles.itemStyle } stackedLabel>
                  <Label>Url</Label>
                  <Input onChangeText={ ( text ) => textValueChanged( 'url', text ) } />
                </Item>
                { createError( urlError ) }
              </Form>
            </Body>
          </CardItem>
        </Card>
      <Card>
        <CardItem>
          <Body style={{ alignItems: 'stretch' }}>
            <AutocompleteInput searchText={ data.search_text } searchTextChanged={ props.searchTextChanged } onPersonSelected={ props.onPersonSelected } results={ filteredUsers } />
            <View style={ styles.memberListContainer }>
              <MemberList onAttendeeRemoved={ onAttendeeRemoved } attendees={ data.attendees } />
            </View>
          </Body>
        </CardItem>
      </Card>
    </Content>
    <FloatingButton onPress={ props.onSave } color="#4CAF50" icon="checkmark" />
  </Container>;
}

AddEditMeeting.defaultProps = {
  canDisplayErrors: false,
  nameError: false,
  agendaError: false,
  locationError: false,
  urlError: false,
};

export default AddEditMeeting;
