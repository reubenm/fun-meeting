import React, { Component } from 'react'
import { View, Image, TouchableOpacity } from 'react-native'
import { Icon } from 'native-base';
import styles from './Styles/MainHeaderStyle'

import { Images } from '../Themes'

const MainHeader = ( props ) => {
  return <View style={ styles.container }>
    <View style={ styles.tabButton }>
      <TouchableOpacity style={ styles.touchable } onPress={ () => props.goto( 'my_meetings' ) }>
        <Icon name="calendar" family="FontAwesome" style={{ color: '#D81B60' }} />
      </TouchableOpacity>
    </View>
    <View style={ styles.tabButton }>
      <TouchableOpacity style={ styles.touchable } onPress={ () => props.goto( 'attend_meetings' ) }>
        <Image resizeMode='cover' source={ Images.tabAttendMeetings } style={ styles.iconButtonStyle } />
      </TouchableOpacity>
    </View>
    <View style={ styles.tabButton }>
      <TouchableOpacity style={ styles.touchable } onPress={ () => props.goto( 'my_tasks' ) }>
        <Image resizeMode='cover' source={ Images.tabTasks } style={ styles.iconButtonStyle } />
      </TouchableOpacity>
    </View>
  </View>;
};

export default MainHeader;
