import React, { Component } from "react";
// import PropTypes from 'prop-types';
import { View } from "react-native";
import styles from "./Styles/MeetingViewAgendaStyle";
import {  CardItem,  Left,  Thumbnail,  Body, Text} from "native-base";
import { Images } from "../Themes";
import MeetingItem from './MeetingItem';
import CalendarDateIcon from './CalendarDateIcon';
import moment from "moment";

const MeetingViewAgenda = (props) => {
  const { name, agenda, createdByName, schedule } = props;
  
  const date = moment( schedule );
  return (
    <View style={styles.container}>
      <CardItem>
        <Left>
        <CalendarDateIcon month={ date.format( 'MMMM' ) } day={ date.format( 'DD' ) } />
          <Body>
            <Text style={styles.titleText}>{name}</Text>
            <Text style={styles.subTitleText}>{agenda}</Text>
            <Text note>by {createdByName}</Text>
          </Body>
        </Left>
      </CardItem>
    </View>
  );
}

export default MeetingViewAgenda;

