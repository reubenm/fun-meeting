import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { View, Text } from 'react-native'
import styles from './Styles/CalendarDateIconStyle'

const CalendarDateIcon = ( props ) => {
  return <View style={styles.container}>
    <View style={ styles.monthContainer }>
      <Text style={ styles.monthText }>{ props.month }</Text>
    </View>
    <View style={ styles.dayContainer }>
      <Text style={ styles.dayText }>{ props.day }</Text>
    </View>
  </View>;
}

CalendarDateIcon.propTypes = {
  month: PropTypes.string,
  day: PropTypes.string
};

export default CalendarDateIcon;
