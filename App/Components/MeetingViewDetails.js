import React, { Component } from "react";
import { View } from "react-native";
import styles from "./Styles/MeetingViewDetailsStyle";
import { CardItem, Body, Text } from "native-base";
import moment from "moment";
import Hyperlink from "react-native-hyperlink";

const MeetingViewDetails = props => {
  const { schedule, location, url } = props;
  return (
    <View style={styles.container}>
      <CardItem>
        <Body>
          <Text style={styles.subTitleText}>Details</Text>
          <Text>Time: {moment(schedule).format(" h:mm a")}</Text>
          <Text>Location: {location}</Text>
          <Hyperlink
            linkDefault={true}
            linkStyle={{
              color: "#2980b9",
              textDecorationLine: "underline"
            }}
          >
            <Text>URL: {url}</Text>
          </Hyperlink>
        </Body>
      </CardItem>
    </View>
  );
};

export default MeetingViewDetails;
