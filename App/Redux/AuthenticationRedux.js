import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  authenticationReduxRequest: ['email', 'password', 'navigate'],
  authenticationReduxSuccess: ['payload'],
  authenticationReduxFailure: null,
  checkToken: ['navigate'],
  updateTokenStatus: ['token_status'],
  signOut: ['navigate'],
})

export const AuthenticationTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: false,
  token_status: 'fetching',
})

/* ------------- Selectors ------------- */

export const AuthenticationReduxSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null, error: false });

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: 'done', error: false, payload });
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: 'done', error: true, payload: null });

export const token = ( state, action ) => {
  const { token_status } = action;
  return state.merge( { token_status } );
}

export const signOut = ( state, action ) =>
  state.merge( { data: null, error: false, payload: null, fetching: 'done', token_status: 'done' } );

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.AUTHENTICATION_REDUX_REQUEST]: request,
  [Types.AUTHENTICATION_REDUX_SUCCESS]: success,
  [Types.AUTHENTICATION_REDUX_FAILURE]: failure,
  [Types.UPDATE_TOKEN_STATUS]: token,
  [Types.SIGN_OUT]: signOut,
})
