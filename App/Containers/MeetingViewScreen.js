import React, { Component } from "react";
import { Image } from "react-native";
import { Container, Header, View, Content, Card, Text } from "native-base";
import { ScrollView, KeyboardAvoidingView, Alert } from "react-native";
import { connect } from "react-redux";
import Agenda from "../Components/MeetingViewAgenda";
import Details from "../Components/MeetingViewDetails";
import Attendees from "../Components/MeetingViewAttendees";
import Tasks from "../Components/MeetingViewTasks";
import FloatingButton from "../Components/MeetingViewFab";
//import FloatingButton from '../Components/FloatingButton';

// Styles
import styles from "./Styles/MeetingViewScreenStyle";

class MeetingViewScreen extends Component {
  render() {
    const { item } = this.props.navigation.state.params;
    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior="position">
          <View style={styles.editIconContainer} />

          <Container>
            <Content>
              <Card style={{ flex: 0 }}>
                <Agenda
                  name={item.name}
                  agenda={item.agenda}
                  createdByName={item.created_by.name}
                  schedule={item.schedule}
                />
                <Details
                  schedule={item.schedule}
                  location={item.location}
                  url={item.url}
                />
                <Attendees attendees={item.attendees} />
              </Card>
              <Tasks />
            </Content>
          </Container>
            
            <FloatingButton />
        </KeyboardAvoidingView>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(MeetingViewScreen);
