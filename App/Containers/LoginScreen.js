import React, { Component } from 'react'
import { Alert, ScrollView, Image, View, ActivityIndicator } from 'react-native'
import { Container, Button, Text, Form, Item, Input, Label } from 'native-base';
import { connect } from 'react-redux'

import { Images } from '../Themes';

import Creators, { AuthenticationTypes } from '../Redux/AuthenticationRedux';

// Styles
import styles from './Styles/LoginScreenStyle'

class LoginScreen extends Component {
  constructor ( props ) {
    super( props );

    this.state = {
      email: 'edmondoa@codev.com',
      password: '123456',
      token_status: 'fetching',
    };
  }

  componentDidMount () {
    this.props.checkToken( this.props.navigation.navigate );
  }

  componentWillReceiveProps ( props ) {
    if ( props.fetching === 'done' ) {
      if ( props.error !== false ) {
        Alert.alert( 'Login failed', 'Invalid credentials.' );
      }
    }

    this.setState( { 'token_status' : props.token_status } );
  }

  handleLogin = () => {
    this.props.authenticate( this.state.email, this.state.password, this.props.navigation.navigate );
  }

  textInputChanged = ( field ) => {
    return ( value ) => {
      this.setState( { [field] : value } )
    }
  }

  renderLoadingScreen = () => {
    // Add ActivityIndicator here when we figure out why it's crashing the app
    return <View style={ styles.loadingStyle }>
    </View>;
  }

  render () {
    if ( this.props.token_status === 'fetching' ) {

      return this.renderLoadingScreen();
    }

    return (
      <Container style={ styles.container }>
        <View style={ styles.centerItems }>
          <View style={ styles.backgroundContainer }>
            <Image style={ styles.codevBackground } source={ Images.codev } resizeMode="contain" />
          </View>
          <View style={ styles.formContainer }>
            <Form>
              <Item floatingLabel>
                <Label>Username</Label>
                <Input value={ this.state.email } onChangeText={ this.textInputChanged( 'email' ) } />
              </Item>
              <Item floatingLabel>
                <Label>Password</Label>
                <Input value={ this.state.password } onChangeText={ this.textInputChanged( 'password' ) } secureTextEntry={ true } />
              </Item>
            </Form>
            <View style={ styles.buttonContainer }>
              <Button onPress={ this.handleLogin }>
                <Text>Login</Text>
              </Button>
            </View>
          </View>
        </View>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    'fetching': state.auth.fetching,
    'error': state.auth.error,
    'data': state.auth.data,
    'token_status': state.auth.token_status,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    authenticate ( email, password, navigate ) {
      dispatch( Creators.authenticationReduxRequest( email, password, navigate ) );
    },
    checkToken ( navigate ) {
      dispatch( Creators.checkToken( navigate ) );
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
