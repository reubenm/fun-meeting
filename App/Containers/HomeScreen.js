import React, { Component } from 'react'
import { ScrollView, Text } from 'react-native'
import { connect } from 'react-redux'
import AppHeader from '../Components/AppHeader';

// Styles
import styles from './Styles/HomeScreenStyle'

class HomeScreen extends Component {
  constructor ( props ) {
    super( props );

    this.state = {
      profileImage: require( '../Images/profile.jpg' )
    };
  }

  render () {
    return (
      <ScrollView style={styles.container}>
        <AppHeader profileImage={ this.state.profileImage } />
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
