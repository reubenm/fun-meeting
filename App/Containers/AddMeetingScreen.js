import React, { Component } from 'react'
import Immutable from 'seamless-immutable'
import { Alert, View, Text } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

import AddEditMeeting from '../Components/AddEditMeeting';
import SavingModal from '../Components/SavingModal';

import UsersCreators from '../Redux/UsersRedux';
import MeetingsCreators from '../Redux/MeetingsRedux';

import { filterUsers, attendeeAlreadyAdded } from '../Transforms/Users';
import { validateCreateMeetingData } from '../Transforms/Meetings';

// Styles
import styles from './Styles/AddMeetingScreenStyle'

class AddMeetingScreen extends Component {
  constructor ( props ) {
    super( props );

    this.state = {
      form_submitted: false,
      meeting_create_response: { message: {} },
      users: [],
      search_text: '',
      schedule: new Date(),
      attendees: [],
      name: '',
      agenda: '',
      location: '',
      url: '',
    };
  }

  componentDidMount () {
    this.props.getUsers();
  }

  componentWillReceiveProps ( nextProps ) {
    this.setState( {
      users: nextProps.users,
      meeting_create_response: nextProps.meeting_create_response,
    } );
  }

  dateValueChanged = ( schedule ) => {
    this.setState( { schedule } );
  }

  handlePersonSelected = ( person ) => {
    const { attendees } = this.state;

    if ( !attendeeAlreadyAdded( person, attendees ) ) {
      this.setState( {
        'attendees'   : attendees.concat( [ person ] ),
        'search_text' : ''
      } );
    }
  }

  handleSearchTextChanged = ( search_text ) => {
    this.setState( { search_text } );
  }

  handleTextValuesChanged = ( field, text ) => {
    this.setState( { [field]: text } );
  }

  savePerson = () => {
    const {
      name,
      agenda,
      schedule,
      url,
      location
    } = this.state;

    const data = {
      name,
      agenda,
      schedule,
      url,
      location
    };

    const response = validateCreateMeetingData( data );

    if ( response.error ) {
      this.setState( { 'meeting_create_response' : response, 'form_submitted': true } );
    } else {
      this.setState( {
        form_submitted: true
      } );
      this.props.createMeeting( data );
    }
  }

  handleSave = () => {
    const buttons = [
      { text: 'Yes', onPress: this.savePerson },
      { text: 'No' }
    ];

    Alert.alert( 'Confirm', 'Are you sure you want to save this meeting?', buttons );
  }

  handleAttendeeRemoved = ( UserId ) => {
    const attendees = this.state.attendees.filter( ( attendee ) => {
      return attendee.UserId !== UserId;
    } );

    this.setState( { attendees } );
  }

  render () {
    return (
        <View style={{ flex: 1 }}>
          <AddEditMeeting
            data={ this.state }
            textValueChanged={ this.handleTextValuesChanged }
            filteredUsers={ filterUsers( this.state.search_text, this.state.users ) }
            searchTextChanged={ this.handleSearchTextChanged }
            onPersonSelected={ this.handlePersonSelected }
            onDateValueChanged={ this.dateValueChanged }
            onSave={ this.handleSave }
            onAttendeeRemoved={ this.handleAttendeeRemoved }
            canDisplayErrors={ this.state.form_submitted }
            nameError={ this.state.meeting_create_response.message.name }
            agendaError={ this.state.meeting_create_response.message.agenda }
            locationError={ this.state.meeting_create_response.message.location }
            urlError={ this.state.meeting_create_response.message.url }
            />
          <SavingModal navigation={ this.props.navigation } />
        </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    users: state.users.payload.data || [],
    meeting_create_response: state.meetings.meeting_create_response
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    getUsers () {
      dispatch( UsersCreators.usersRequest() );
    },
    createMeeting ( payload ) {
      dispatch( MeetingsCreators.meetingsCreateRequest( payload ) );
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddMeetingScreen)
