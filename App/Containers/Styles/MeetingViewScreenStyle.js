import { StyleSheet } from 'react-native'
import {Fonts, Metrics, Colors, ApplicationStyles } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  editIconContainer: {
    position: 'absolute',
    top: 0,
    right: -10,
  },
  cardContainer:{
     flex: 0 
  }
})
