/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { AsyncStorage } from 'react-native';
import { call, put } from 'redux-saga/effects'
// import AuthenticationSagaActions from '../Redux/AuthenticationSagaRedux'
import AuthenticationSagaActions from '../Redux/AuthenticationRedux'
// import { AuthenticationSagaSelectors } from '../Redux/AuthenticationSagaRedux'

export function * authenticate (api, action) {
  const data = {
    'email' : action.email,
    'password' : action.password,
  };

  // get current data from Store
  // const currentData = yield select(AuthenticationSagaSelectors.getData)
  // make the call to the api
  try {
    const response = yield call(api.authenticate, data)
    // success?
    if (response.ok) {
      // You might need to change the response here - do this with a 'transform',
      // located in ../Transforms/. Otherwise, just pass the data back from the api.
      yield put(AuthenticationSagaActions.authenticationReduxSuccess(response.data));
      yield call(api.root_api.setHeader, 'Authorization', `Bearer ${response.data.data.token}`);
      yield call(AsyncStorage.setItem, 'token', response.data.data.token);
      yield call(action.navigate, 'MyMeetings');
    } else {
      yield put(AuthenticationSagaActions.authenticationReduxFailure())
    }
  } catch ( ex ) {
    yield put(AuthenticationSagaActions.authenticationReduxFailure())
  }
}

export function * checkExistingToken ( api, action ) {
  try {
    const value = yield call(AsyncStorage.getItem, 'token');


    if ( value !== null ) {
      yield call(api.root_api.setHeader, 'Authorization', `Bearer ${value}`);

      // Perform a test query to see if the token is still working
      // We might need to replace this with a call to a simpler test route
      const response = yield call(api.getmeetings);

      if ( response.ok ) {
        yield call(action.navigate, 'MyMeetings');
      } else {
      }

    } else {
    }
  } catch ( error ) {
  }

  yield put( AuthenticationSagaActions.updateTokenStatus( 'done' ) );
}

export function * signOut ( action ) {
  const { navigate } = action;

  yield call(AsyncStorage.removeItem, 'token');
  yield call(action.navigate, 'LoginScreen');
}
