import { takeLatest, takeEvery, all } from 'redux-saga/effects'
import API from '../Services/Api'
import FixtureAPI from '../Services/FixtureApi'
import DebugConfig from '../Config/DebugConfig'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { GithubTypes } from '../Redux/GithubRedux'
import { AuthenticationTypes } from '../Redux/AuthenticationRedux'
import { MeetingsTypes } from '../Redux/MeetingsRedux'
import { UsersTypes } from '../Redux/UsersRedux'
import { DrawerTypes } from '../Redux/DrawerRedux'

/* ------------- Sagas ------------- */

import { startup } from './StartupSagas';
import { getUserAvatar } from './GithubSagas';
import { authenticate, checkExistingToken, signOut } from './AuthenticationSagaSagas';
import { getMeetings, createMeeting, createMeetingReset } from './MeetingsSagas';
import { getUsers } from './UsersSagas';
import { toggleDrawer } from './DrawerSagas';

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugConfig.useFixtures ? FixtureAPI : API.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    takeEvery( AuthenticationTypes.AUTHENTICATION_REDUX_REQUEST, authenticate, api ),
    takeEvery( AuthenticationTypes.CHECK_TOKEN, checkExistingToken, api ),
    takeEvery( AuthenticationTypes.SIGN_OUT, signOut ),
    takeEvery( MeetingsTypes.MEETINGS_REQUEST, getMeetings, api ),
    takeEvery( UsersTypes.USERS_REQUEST, getUsers, api ),
    takeEvery( MeetingsTypes.MEETINGS_CREATE_REQUEST, createMeeting, api ),
    takeEvery( MeetingsTypes.MEETINGS_CREATE_REQUEST_RESET, createMeetingReset ),
    // takeEvery( DrawerTypes.TOGGLE_DRAWER, toggleDrawer ),
  ])
}
