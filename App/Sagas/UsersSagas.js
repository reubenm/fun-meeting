/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import UsersActions from '../Redux/UsersRedux'
// import { UsersSelectors } from '../Redux/UsersRedux'

export function * getUsers (api, action) {
  const { data } = action;
  try {
  const response = yield call(api.getusers, data)

    // success?
    if (response.ok) {
      yield put(UsersActions.usersSuccess(response.data))
    } else {
      yield put(UsersActions.usersFailure())
    }

  } catch ( ex ) {
    yield put(UsersActions.usersFailure())
  }
}
