import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes/'

export default StyleSheet.create({
  headerStyle: {
    backgroundColor: 'rgb(0, 188, 212)'
  },
  headerTitleStyle: {
    color: '#fff'
  }
})
